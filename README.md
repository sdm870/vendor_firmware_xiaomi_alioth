Getting Started (Current version - V13.0.4.0.SKHINXM)
---------------

Clone firmware repo

    git clone https://gitlab.com/sdm870/vendor_firmware_xiaomi_alioth vendor/firmware/xiaomi/alioth

Add this line in BoardConfig

    include vendor/firmware/xiaomi/alioth/BoardConfigVendor.mk

